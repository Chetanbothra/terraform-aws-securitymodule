resource "aws_budgets_budget" "fifty_percent" {
    count = var.enable_aws_budgets_budget ? 1 : 0
    name              = "${var.stack}-fifty-percent-monthly"
    budget_type       = "COST"
    limit_amount      = format("%s.0", element(split(".", var.monthly_budget), 0))
    limit_unit        = "USD"
    time_period_end   = "2087-06-15_00:00"
    time_period_start = "2017-07-01_00:00"
    time_unit         = "MONTHLY"

    notification {
        comparison_operator        = "GREATER_THAN"
        threshold                  = 50
        threshold_type             = "PERCENTAGE"
        notification_type          = "ACTUAL"
        subscriber_email_addresses = [var.responsible_party_email]
}
}

resource "aws_budgets_budget" "sixty_percent" {
    count = var.enable_aws_budgets_budget ? 1 : 0
    name              = "${var.stack}-sixty-percent-monthly"
    budget_type       = "COST"
    limit_amount      = format("%s.0", element(split(".", var.monthly_budget), 0))
    limit_unit        = "USD"
    time_period_end   = "2087-06-15_00:00"
    time_period_start = "2017-07-01_00:00"
    time_unit         = "MONTHLY"

    notification {
        comparison_operator        = "GREATER_THAN"
        threshold                  = 60
        threshold_type             = "PERCENTAGE"
        notification_type          = "ACTUAL"
        subscriber_email_addresses = [var.responsible_party_email]
}
}

resource "aws_budgets_budget" "seventy_percent" {
    count = var.enable_aws_budgets_budget ? 1 : 0
    name              = "${var.stack}-seventy-percent-monthly"
    budget_type       = "COST"
    limit_amount      = format("%s.0", element(split(".", var.monthly_budget), 0))
    limit_unit        = "USD"
    time_period_end   = "2087-06-15_00:00"
    time_period_start = "2017-07-01_00:00"
    time_unit         = "MONTHLY"

    notification {
        comparison_operator        = "GREATER_THAN"
        threshold                  = 70
        threshold_type             = "PERCENTAGE"
        notification_type          = "ACTUAL"
        subscriber_email_addresses = [var.responsible_party_email]
}
}

resource "aws_budgets_budget" "eighty_percent" {
    count = var.enable_aws_budgets_budget ? 1 : 0
    name              = "${var.stack}-eighty-percent-monthly"
    budget_type       = "COST"
    limit_amount      = format("%s.0", element(split(".", var.monthly_budget), 0))
    limit_unit        = "USD"
    time_period_end   = "2087-06-15_00:00"
    time_period_start = "2017-07-01_00:00"
    time_unit         = "MONTHLY"

    notification {
        comparison_operator        = "GREATER_THAN"
        threshold                  = 80
        threshold_type             = "PERCENTAGE"
        notification_type          = "ACTUAL"
        subscriber_email_addresses = [var.responsible_party_email]
}
}

resource "aws_budgets_budget" "ninety_percent" {
    count = var.enable_aws_budgets_budget ? 1 : 0
    name              = "${var.stack}-ninety-percent-monthly"
    budget_type       = "COST"
    limit_amount      = format("%s.0", element(split(".", var.monthly_budget), 0))
    limit_unit        = "USD"
    time_period_end   = "2087-06-15_00:00"
    time_period_start = "2017-07-01_00:00"
    time_unit         = "MONTHLY"

    notification {
        comparison_operator        = "GREATER_THAN"
        threshold                  = 90
        threshold_type             = "PERCENTAGE"
        notification_type          = "ACTUAL"
        subscriber_email_addresses = [var.responsible_party_email, var.admin_group_email]
}
}

resource "aws_budgets_budget" "hundred_percent" {
    count = var.enable_aws_budgets_budget ? 1 : 0
    name              = "${var.stack}-hundred-percent-monthly"
    budget_type       = "COST"
    limit_amount      = format("%s.0", element(split(".", var.monthly_budget), 0))
    limit_unit        = "USD"
    time_period_end   = "2087-06-15_00:00"
    time_period_start = "2017-07-01_00:00"
    time_unit         = "MONTHLY"

    notification {
        comparison_operator        = "GREATER_THAN"
        threshold                  = 100
        threshold_type             = "PERCENTAGE"
        notification_type          = "ACTUAL"
        subscriber_email_addresses = [var.responsible_party_email, var.admin_group_email]
}
}

resource "aws_budgets_budget" "forecasted_above_budget" {
    count = var.enable_aws_budgets_budget ? 1 : 0
    name              = "${var.stack}-forecasted-above-monthly"
    budget_type       = "COST"
    limit_amount      = format("%s.0", element(split(".", var.monthly_budget), 0))
    limit_unit        = "USD"
    time_period_end   = "2087-06-15_00:00"
    time_period_start = "2017-07-01_00:00"
    time_unit         = "MONTHLY"

    notification {
        comparison_operator        = "GREATER_THAN"
        threshold                  = 100
        threshold_type             = "PERCENTAGE"
        notification_type          = "FORECASTED"
        subscriber_email_addresses = [var.responsible_party_email]
}
}