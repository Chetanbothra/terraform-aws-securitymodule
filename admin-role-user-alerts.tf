resource "aws_cloudwatch_log_metric_filter" "filter" {
  name           = "${var.stack}-${var.environment}-AdminUsage"
  pattern        = <<PATTERN
  { ($.requestParameters.policyArn = "arn:aws:iam::aws:policy/AdministratorAccess") && ($.eventName = "AttachRolePolicy") }
  PATTERN
  log_group_name = "aws-cloudtrail-logs-for-adminalert"

  metric_transformation {
    name      = "AdminUsageCount"
    namespace = "CloudTrailMetrics"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "admin_usage_count" {
  alarm_name          = "${var.stack}-${var.environment}-admin-usage-count"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "AdminUsageCount"
  namespace           = "CloudTrailMetrics"
  period              = "30"
  statistic           = "Average"
  threshold           = "1"
  alarm_description   = "Alert when Admin role is attached with any user"
  alarm_actions       = [aws_sns_topic_policy.PolicyForSnsTopic.arn]
  ok_actions          = []

}
