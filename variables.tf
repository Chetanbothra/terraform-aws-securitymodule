#################################
# General Variables             #
#################################

variable "stack" {
    type = string
}
variable "environment" {
    type = string
}

#################################
# Billing Variables             #
#################################
variable "enable_aws_budgets_budget" {
    description = "if we need to Enable we have to give True otherwise false"
    type = bool
    default = true
}

variable "monthly_budget" {
    description = "Amount of budget per month to alert on"
    type        = string
}

variable "responsible_party_email" {
    description = "Email address used for notification of budget spend"
    type        = string
}

variable "admin_group_email" {
    description = "Email address of admin group"
    type        = string
}

#################################
# SNS Variables                 #
#################################

variable "email_id_for_alert" {
    description = "Email id for SNS alert" 
    type        = string
}

############################
# USER_RESTRICT            #
#                         #
###########################

variable "username" {
    type = string
}
