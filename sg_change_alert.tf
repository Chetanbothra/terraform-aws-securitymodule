resource "aws_cloudwatch_event_rule" "EventRule" {
  name = "${var.stack}-${var.environment}-detect-security-group-changes"
  description = "A CloudWatch Event Rule that detects changes to security groups and publishes change events to an SNS topic for notification."
  is_enabled = true
  event_pattern = <<PATTERN
{
  "detail-type": [
    "AWS API Call via CloudTrail"
  ],
  "detail": {
    "eventSource": [
      "ec2.amazonaws.com"
    ],
    "eventName": [
      "AuthorizeSecurityGroupIngress",
      "AuthorizeSecurityGroupEgress",
      "RevokeSecurityGroupIngress",
      "RevokeSecurityGroupEgress",
      "CreateSecurityGroup",
      "DeleteSecurityGroup"
    ]
  }
}
PATTERN

}

resource "aws_cloudwatch_event_target" "TargetForEventRule" {
  rule = aws_cloudwatch_event_rule.EventRule.name
  target_id = "SendToSNS"
  arn = aws_sns_topic.alerts_ec2_state_change.arn
}
